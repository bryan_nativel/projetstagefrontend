<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class QuapeDiagnosticMedicalController extends AbstractController
{
    /**
     * @Route("/connection", name="connection")
     */
    public function login()
    {
        return $this->render('quape_diagnostic_medical/login.html.twig');
    }
    
    /**
     * @Route("/accueill", name="accueil")
     */
    public function homeLogout()
    {
        return $this->render('quape_diagnostic_medical/homeLogout.html.twig');
    }

    /**
     * @Route("/", name="home")
     */
    public function homeLogin()
    {
        return $this->render('quape_diagnostic_medical/home.html.twig');

    }

    /**
     * @Route("/profile", name="profile")
     */
    public function userProfile()
    {
        return $this->render('quape_diagnostic_medical/userProfile.html.twig');
    }

    /**
     * @Route("/formDiagnostic", name="formDiagnostic")
     */
    public function formDiagnostic()
    {
        return $this->render('quape_diagnostic_medical/formDiagnostic.html.twig');
    }
}
