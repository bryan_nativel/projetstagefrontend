import axios from "axios";

export const koversClient = {
  tokens: "",

  loginClient(user) {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        data: user,
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(resp => {
          const token = resp.data.token;
          this.tokens = token;
          if (token != null) resolve(token);
          else reject("token_vide");
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  getUserData() {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + this.tokens
        }
      })
        .then(resp => {
          const dataUserProfile = resp.data;
          if (dataUserProfile != "") resolve(dataUserProfile);
          else reject("dataUser vide");
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  forgetAdherentNumber(email) {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        data: email,
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(resp => {
          const responce = resp.data;
          if (responce != "") resolve(responce);
          else reject("Erreur email invalid");
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  forgetPassword(userName) {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        data: userName,
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(resp => {
          const responce = resp.data;
          if (responce != "") resolve(responce);
          else reject("userName invalid");
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};
