import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import VRuntimeTemplate from 'v-runtime-template';
import VTooltip from 'v-tooltip';

import { store } from './store/store';

import navbar from './components/navBar';
import southbar from './components/footer';
import formlogin from './components/formLogin';
import userprofile from './components/userProfile';
import forgetforms from './components/formForget';
import home from './components/home';
import formdiagnostic from './components/formsDiagnostic';

Vue.use(Vuetify);
Vue.use(VTooltip);

new Vue({
	store,
	el: '#bodyFront',

	components: {
		navbar,
		southbar,
		formlogin,
		userprofile,
		forgetforms,
		home,
		formdiagnostic
	},

	vuetify: new Vuetify()
});
