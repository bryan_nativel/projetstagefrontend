import Vue from 'vue';
import Vuex from 'vuex';

import { login } from './login';
import { userProfile } from './userProfile';
import { forgetAdherentNumber } from './forgetAdherentNumber';
import { forgetPassword } from './forgetPassword';
import { diagnostic } from './diagnostic';

Vue.use(Vuex);

export const store = new Vuex.Store({
	modules: {
		login,
		userProfile,
		forgetAdherentNumber,
		forgetPassword,
		diagnostic
	}
});
