import { diagnosticClient } from "../diagnosticClient";

let strict = {
  strict: true
};

let state = {
  /* Peux avoir un bug ici  */
  sessionId: localStorage.getItem("sessionId") || "",
  questions: JSON.parse(localStorage.getItem("questions")) || {},
  nextService: JSON.parse(localStorage.getItem("nextService")) || []
};

let mutations = {
  reset(state) {
    localStorage.removeItem("sessionId");
    localStorage.removeItem("nextService");
    localStorage.removeItem("questions");

    state.questions = {};
    state.sessionId = "";
    state.nextService = [];
  },
  session_success(state, sessionId) {
    localStorage.setItem("sessionId", sessionId);
    state.sessionId = sessionId;
  },
  nextService_success(state, nextService) {
    localStorage.setItem("nextService", JSON.stringify(nextService));
    state.nextService = nextService;
    localStorage.removeItem("questions");
    state.questions = {};
  },
  questions_success(state, questions) {
    localStorage.setItem("questions", JSON.stringify(questions));
    state.questions = questions;
  }
};

let actions = {
  sessionStartDiag({ commit }, data) {
    commit("reset");
    return new Promise((resolve, reject) => {
      diagnosticClient
        .sessionStartDiagClient(data)
        .then(sessionId => {
          commit("session_success", sessionId);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  sessionPingDiag({ commit }) {
    return new Promise((resolve, reject) => {
      diagnosticClient
        .sessionPingDiagClient(state.sessionId)
        .then(nextService => {
          commit("nextService_success", nextService);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  loadQuestionsDiag({ commit }, url) {
    if (Object.entries(state.questions).length == 0) {
      return new Promise((resolve, reject) => {
        diagnosticClient
          .loadQuestionsDiagClient(state.sessionId, url)
          .then(questions => {
            commit("questions_success", questions);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    }
  },
  setAnswereDiag({ commit }, answere) {
    return new Promise((resolve, reject) => {
      let answeres = JSON.stringify(answere);
      diagnosticClient
        .setAnswereDiagClient(state.sessionId, answeres)
        .then(nextService => {
          commit("nextService_success", nextService);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};

let getters = {
  isLogged: state => state.sessionId != ""
};

export const diagnostic = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
  strict
};
