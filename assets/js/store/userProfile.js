import { koversClient } from '../koversClient';

let strict = {
	strict: true
};

let state = {
	dataUser: JSON.parse(localStorage.getItem('dataUser')) || null,
	dataStatus: false
};

let mutations = {
	data_success(state, dataUserProfile) {
		state.dataStatus = true;
		state.dataUser = dataUserProfile;

		/* Le localStorage à pour but de socker la dataUserProfile
         en cas de changement page ou fermeture du navigateur   */
		localStorage.setItem('dataUser', JSON.stringify(dataUserProfile));
	},

	data_error(state) {
		state.dataStatus = false;
	}
};

let actions = {
	userProfile({ commit }) {
		return new Promise((resolve, reject) => {
			koversClient
				.getUserData()
				.then((dataUserProfile) => {
					commit('data_success', dataUserProfile.data);
					resolve();
				})
				.catch((err) => {
					commit('data_error');
					reject(err);
				});
		});
	}
};

let getters = {
	isGeting: (state) => state.dataUser != ''
};

export const userProfile = {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
	strict
};
