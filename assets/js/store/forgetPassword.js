import { koversClient } from '../koversClient';

let strict = {
	strict: true
};

let actions = {
	forgetPassword({}, userName) {
		return new Promise((resolve, reject) => {
			koversClient
				.forgetPassword(userName)
				.then(() => {
					resolve();
				})
				.catch((err) => {
					reject(err);
				});
		});
	}
};

export const forgetPassword = {
	namespaced: true,
	actions,
	strict
};
