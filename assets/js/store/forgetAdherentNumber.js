import { koversClient } from '../koversClient';

let strict = {
	strict: true
};

let actions = {
	forgetAdherentNumber({}, email) {
		return new Promise((resolve, reject) => {
			koversClient
				.forgetAdherentNumber(email)
				.then(() => {
					resolve();
				})
				.catch((err) => {
					reject(err);
				});
		});
	}
};

export const forgetAdherentNumber = {
	namespaced: true,
	actions,
	strict
};
