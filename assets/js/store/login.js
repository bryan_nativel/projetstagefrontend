import axios from "axios";
import { koversClient } from "../koversClient";

let strict = {
  strict: true
};

let state = {
  loading: false,
  token: localStorage.getItem("token") || ""
};

let mutations = {
  auth_request_start(state) {
    state.loading = true;
  },
  auth_success(state, token) {
    localStorage.setItem("token", token);
    axios.defaults.headers.common["Authorization"] = token;
    state.loading = false;
    state.token = token;
  },
  auth_error(state) {
    /*  commit('logout') */
    state.loading = false;
  },
  logout(state) {
    state.token = "";
    localStorage.removeItem("token");
    localStorage.removeItem("dataUser");
    delete axios.defaults.headers.common["Authorization"];
  }
};

let actions = {
  loginStore({ commit }, user) {
    return new Promise((resolve, reject) => {
      commit("auth_request_start");
      koversClient
        .loginClient(user)
        .then(token => {
          commit("auth_success", token);
          resolve();
        })
        .catch(err => {
          commit("auth_error");
          reject(err);
        });
    });
  },

  logout({ commit }) {
    return new Promise((resolve, reject) => {
      commit("logout");
      resolve();
    });
  }
};

let getters = {
  isLogged: state => state.token != "",
  isLoading: state => state.loading
};

export const login = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
  strict
};
