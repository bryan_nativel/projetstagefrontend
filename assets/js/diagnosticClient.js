import axios from "axios";

export const diagnosticClient = {
  sessionStartDiagClient(data) {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        data: data,
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
        .then(resp => {
          const sessionId = resp.data.id;
          if (sessionId != "") resolve(sessionId);
          else reject("sessionId_vide");
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  sessionPingDiagClient(sessionId) {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          SessionId: sessionId
        }
      })
        .then(resp => {
          const nextService = resp.data.nextServices;
          if (nextService != null) resolve(nextService);
          else reject("nextService not found");
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  loadQuestionsDiagClient(sessionId, url) {
    return new Promise((resolve, reject) => {
      axios({
        url: url,
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          SessionId: sessionId
        }
      })
        .then(resp => {
          const questions = resp.data;
          this.question = questions;
          if (questions != null) resolve(questions);
          else reject("questions not found");
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  setAnswereDiagClient(sessionId, answeres) {
    return new Promise((resolve, reject) => {
      axios({
        url: "API",
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          SessionId: sessionId
        },
        data: answeres
      })
        .then(resp => {
          const nextService = resp.data.nextServices;
          if (nextService != null) resolve(nextService);
          else reject("nextService not found");
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};
